// app.js

const express = require('express');
const app = express();
const host = '0.0.0.0';
const port = 3000;

app.get('/', (req, res) => {
  res.send('Hello, this is my Node.js microservice!');
});

app.listen(port, host, () => {
  console.log(`Server is listening at http://${host}:${port}`);
});
